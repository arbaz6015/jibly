<?php
  session_start(); 

if(isset($_SESSION['id_traveler']) || isset($_SESSION['id_sender'])) { 
  header("Location: index.php");
  exit();
}

?>
<!DOCTYPE html>
<html>

<?php include('header.php'); ?>
<body class="hold-transition login-page">
<div class="wp-block-uagb-container uagb-block-79e82c98 alignfull uagb-is-root-container">
  <div class="uagb-container-inner-blocks-wrap">
<div class="wp-block-uagb-container uagb-block-3035d81c">
<div class="wp-block-uagb-image uagb-block-3fe0b2c7 wp-block-uagb-image--layout-default wp-block-uagb-image--effect-static wp-block-uagb-image--align-none"> 
  <div class="content-wrapper" style="margin-left: 0px;">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php"><b>Jibly.io</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sender Login</p>

    <form method="post" action="checksenderlogin.php">
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- <div class="col-xs-8">
          <a href="#">I forgot my password</a>
        </div> -->
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
        <div class="col-xs-12">
        <?php 
              //If Sender have successfully registered then show them this success message
              //Todo: Remove Success Message without reload?
              if(isset($_SESSION['registerCompleted'])) {
                ?>
                <div>
                  <p class="text-center">You Have Registered Successfully! </p>
                </div>
              <?php
               unset($_SESSION['registerCompleted']); }
              ?>   
              <?php 
              //If Sender Failed To log in then show error message.
              if(isset($_SESSION['loginError'])) {
                ?>
                <div>
                  <p class="text-center">Invalid Email/Password! Try Again!</p>
                </div>
              <?php
               unset($_SESSION['loginError']); }
              ?>   
              <?php 
              if(isset($_SESSION['senderLoginError'])) {
                ?>
                <div>
                  <p class="text-center"><?php echo $_SESSION['senderLoginError'] ?></p>
                </div>
              <?php
               unset($_SESSION['senderLoginError']); }
              ?>  
          </div>          
      </div>
    </form>

    <br>

  </div>
  <!-- /.login-box-body -->
</div>
  </div></div></div></div></div>
<!-- /.login-box -->
<?php include('footer.php'); ?>

<!-- jQuery 3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
