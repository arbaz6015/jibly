<?php

//To Handle Session Variables on This Page
session_start();

//Including Database Connection From db.php file to avoid rewriting in all files
require_once("db.php");

//If user Actually clicked register button
if(isset($_POST)) 
{	
	// echo '<pre>';
	// var_dump($_FILES);
	// print($_FILES); exit;
	//Escape Special Characters In String First
	$firstname = mysqli_real_escape_string($conn, $_POST['fname']);
	$lastname = mysqli_real_escape_string($conn, $_POST['lname']);
	$address = mysqli_real_escape_string($conn, $_POST['address']);
	$city = mysqli_real_escape_string($conn, $_POST['city']);
	$dob = mysqli_real_escape_string($conn, $_POST['dob']);
	$state = mysqli_real_escape_string($conn, $_POST['state']);
	$contactno = mysqli_real_escape_string($conn, $_POST['contactno']);
	$aboutme = mysqli_real_escape_string($conn, $_POST['aboutme']);
	$email = mysqli_real_escape_string($conn, $_POST['email']);
	$password = mysqli_real_escape_string($conn, $_POST['password']);
	$password = base64_encode(strrev(md5($password)));

	// $age = mysqli_real_escape_string($conn, $_POST['age']);

	//sql query to check if email already exists or not
	$sql = "SELECT `email` FROM `traveler` WHERE `email`='$email'";
	$result = $conn->query($sql);
	//if email not found then we can insert new data
	if($result->num_rows == 0) 
	{
		$uploadOk = true;
		
		$folder_dir = "uploads/logo/";
		
		$base = basename($_FILES['traveler_picture']['name']); 

		$imageFileType = pathinfo($base, PATHINFO_EXTENSION); 
		
		$file = uniqid() . "." . $imageFileType; 
		
		$filename = $folder_dir.$file;  
		
		//We check if file is saved to our temp location or not.
		if(file_exists($_FILES['traveler_picture']['tmp_name'])) 
		{ 
			
			if($imageFileType == "jpg" || $imageFileType == "png")  
			{
				if($_FILES['traveler_picture']['size'] < 500000) 
				{ 
					move_uploaded_file($_FILES["traveler_picture"]["tmp_name"], $filename);
				} 
				else 
				{
					$_SESSION['uploadError'] = "Wrong Size. Max Size Allowed : 5MB";
					$uploadOk = false;
				}
			} 
			else 
			{
				//Format Error
				$_SESSION['uploadError'] = "Wrong Format. Only jpg & png Allowed";
				$uploadOk = false;
			}
		} 
		else 
		{
			//File not copied to temp location error.
			$_SESSION['uploadError'] = "Profile picture could not be uploaded. Please try again.";
			$uploadOk = false;
		}

		//If there is any error then redirect back.
		if($uploadOk == false) {
			header("Location: register-traveler.php");
			exit();
		}

		$hash = md5(uniqid());

		//sql new registration insert query
		$sql = "INSERT INTO `traveler`(`firstname`, `lastname`, `email`, `password`, `address`, `city`, `state`, `contactno`, `profile_picture`, `dob`, `hash`, `aboutme`) VALUES ('$firstname', '$lastname', '$email', '$password', '$address', '$city', '$state', '$contactno', '$file', '$dob','$hash', '$aboutme')";

		if($conn->query($sql) === TRUE) 
		{
			// Send Email

			// $to = $email;

			// $subject = "Job Portal - Confirm Your Email Address";

			// $message = '
			
			// <html>
			// <head>
			// 	<title>Confirm Your Email</title>
			// <body>
			// 	<p>Click Link To Confirm</p>
			// 	<a href="yourdomain.com/verify.php?token='.$hash.'&email='.$email.'">Verify Email</a>
			// </body>
			// </html>
			// ';

			// $headers[] = 'MIME-VERSION: 1.0';
			// $headers[] = 'Content-type: text/html; charset=iso-8859-1';
			// $headers[] = 'To: '.$to;
			// $headers[] = 'From: hello@yourdomain.com';
			// //you add more headers like Cc, Bcc;

			// $result = mail($to, $subject, $message, implode("\r\n", $headers)); // \r\n will return new line. 

			// if($result === TRUE) 
			// {

				//If data inserted successfully then Set some session variables for easy reference and redirect to login

			// }

			// //If data inserted successfully then Set some session variables for easy reference and redirect to login
			//If data inserted successfully then Set some session variables for easy reference and redirect to login

			$_SESSION['registerCompleted'] = 'Your traveler account has successfully been created!';
			header("Location: login-traveler.php");
			exit();
		} 
		else 
		{
			//If data failed to insert then show that error. Note: This condition should not come unless we as a developer make mistake or someone tries to hack their way in and mess up :D
			echo "Error " . $sql . "<br>" . $conn->error;
		}
	} 
	else 
	{
		//if email found in database then show email already exists error.
		$_SESSION['registerError'] = true;
		header("Location: register-traveler.php");
		exit();
	}
	//Close database connection. Not compulsory but good practice.
	$conn->close();
} 
else 
{
	//redirect them back to register page if they didn't click register button
	header("Location: register-traveler.php");
	exit();
}