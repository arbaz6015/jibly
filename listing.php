<?php

//To Handle Session Variables on This Page
session_start();


//Including Database Connection From db.php file to avoid rewriting in all files
require_once("db.php");
?>
<!DOCTYPE html>
<html>
<?php 
  // include('header-top.php');
?>
<?php include('header.php'); ?>
<body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper">
    

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-left: 0px;">

      <section class="content-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12 latest-listing margin-top-50 margin-bottom-20">
            <h1 class="text-center">Latest Listings</h1>  
              <div class="input-group input-group-lg">
                  <input type="text" id="searchBar" class="form-control" placeholder="Search Listing">
                  <span class="input-group-btn">
                    <button id="searchBtn" type="button" class="btn btn-info btn-flat">Go!</button>
                  </span>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="candidates" class="content-header">
        <div class="container">
          <div class="row">
            <!-- <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Filters</h3>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked tree" data-widget="tree">
                    <li class="treeview menu-open">
                      <a href="#"><i class="fa fa-plane text-red"></i> City <span class="pull-right"><i class="fa fa-angle-down pull-right"></i></span></a>
                      <ul class="treeview-menu">
                        <li><a href=""  class="citySearch" data-target="Bengaluru"><i class="fa fa-circle-o text-yellow"></i> Bengaluru</a></li>
                        <li><a href="" class="citySearch" data-target="Navi Mumbai"><i class="fa fa-circle-o text-yellow"></i> Navi Mumbai</a></li>
                      </ul>
                    </li>
                    <li class="treeview menu-open">
                      <a href="#"><i class="fa fa-plane text-red"></i> Experience <span class="pull-right"><i class="fa fa-angle-down pull-right"></i></span></a>
                      <ul class="treeview-menu">
                        <li><a href="" class="experienceSearch" data-target='1'><i class="fa fa-circle-o text-yellow"></i> > 1 Years</a></li>
                        <li><a href="" class="experienceSearch" data-target='2'><i class="fa fa-circle-o text-yellow"></i> > 2 Years</a></li>
                        <li><a href="" class="experienceSearch" data-target='3'><i class="fa fa-circle-o text-yellow"></i> > 3 Years</a></li>
                        <li><a href="" class="experienceSearch" data-target='4'><i class="fa fa-circle-o text-yellow"></i> > 4 Years</a></li>
                        <li><a href="" class="experienceSearch" data-target='5'><i class="fa fa-circle-o text-yellow"></i> > 5 Years</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div> -->
            </div>

            <div class="col-md-12">
              <div class="container">
                <div class="row">
                  <div class="col-md-12 latest-listing margin-bottom-20">
                    <?php 
                    /* Show any 4 random job post
                    * 
                    * Store sql query result in $result variable and loop through it if we have any rows
                    * returned from database. $result->num_rows will return total number of rows returned from database.
                    */

                    $sql = "SELECT * FROM `listing` WHERE `status`='ACTIVE' Order By `id_listing`";

                    $result = $conn->query($sql);
                    if($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) 
                      {
                        $sql1 = "SELECT * FROM `sender` WHERE `id_sender`='$row[id_sender]'";
                        $result1 = $conn->query($sql1);
                        if($result1->num_rows > 0) 
                        {
                          while($row1 = $result1->fetch_assoc()) 
                          { 
                            if(!empty($row['item_picture'])){
                              $item_picture = $row['item_picture'];
                              $item_picture = explode(",",$row['item_picture'])[0];
                            }
                            else{
                              $item_picture = 'img/item.jpg';
                            }
                      ?>
                      <div class="attachment-block clearfix">
                        <img class="attachment-img" src="<?= $item_picture;?>" alt="Attachment Image">
                        <div class="attachment-pushed">
                          <h4 class="attachment-heading"><a href="view-listing.php?id=<?php echo $row['id_listing']; ?>"><?php echo $row['listing_title']; ?></a> <span class="attachment-heading pull-right">Posted On <?php echo date("d-M-Y H:i:s", strtotime($row['createdat'])); ?></span></h4>
                          <div class="attachment-text">
                              <div><strong>From : <?= $row['pickup_address']; ?> | To : <?= $row['dropoff_address']; ?> | Item Weight: <?= $row['item_weight']; ?> lbs</strong></div>
                          </div>
                        </div>
                      </div>
                    <?php
                      }
                    }
                    }
                  }
                  ?>
                  </div>
                </div>
              </div> 
            </div>
          </div>
        </div>
      </section>

    </div>
    <!-- /.content-wrapper -->

    <?php include('footer.php'); ?>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="js/adminlte.min.js"></script>
  <script src="js/jquery.twbsPagination.min.js"></script>

  <script>
    function Pagination() {
      $("#pagination").twbsPagination({
        totalPages: <?php echo $total_pages; ?>,
        visible: 5,
        onPageClick: function (e, page) {
          e.preventDefault();
          $("#target-content").html("loading....");
          $("#target-content").load("jobpagination.php?page="+page);
        }
      });
    }
  </script>

  <script>
    $(function () {
        Pagination();
    });
  </script>

  <script>
    $("#searchBtn").on("click", function(e) {
      e.preventDefault();
      var searchResult = $("#searchBar").val();
      var filter = "searchBar";
      if(searchResult != "") {
        $("#pagination").twbsPagination('destroy');
        Search(searchResult, filter);
      } else {
        $("#pagination").twbsPagination('destroy');
        Pagination();
      }
    });
  </script>

  <script>
    $(".experienceSearch").on("click", function(e) {
      e.preventDefault();
      var searchResult = $(this).data("target");
      var filter = "item_weight";
      if(searchResult != "") {
        $("#pagination").twbsPagination('destroy');
        Search(searchResult, filter);
      } else {
        $("#pagination").twbsPagination('destroy');
        Pagination();
      }
    });
  </script>

  <script>
    $(".citySearch").on("click", function(e) {
      e.preventDefault();
      var searchResult = $(this).data("target");
      var filter = "city";
      if(searchResult != "") {
        $("#pagination").twbsPagination('destroy');
        Search(searchResult, filter);
      } else {
        $("#pagination").twbsPagination('destroy');
        Pagination();
      }
    });
  </script>

  <script>
    function Search(val, filter) {
      $("#pagination").twbsPagination({
        totalPages: <?php echo $total_pages; ?>,
        visible: 5,
        onPageClick: function (e, page) {
          e.preventDefault();
          val = encodeURIComponent(val);
          $("#target-content").html("loading....");
          $("#target-content").load("search.php?page="+page+"&search="+val+"&filter="+filter);
        }
      });
    }
  </script>
</body>
</html>
