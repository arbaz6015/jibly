<?php 

session_start();

if(isset($_SESSION['id_traveler']) || isset($_SESSION['id_sender'])) { 
  header("Location: index.php");
  exit();
}

?>
<!DOCTYPE html>
<html>

<!-- <body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper"> -->
    <?php include('header.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="wp-block-uagb-container uagb-block-79e82c98 alignfull uagb-is-root-container">
      <div class="uagb-container-inner-blocks-wrap">
        <div class="wp-block-uagb-container uagb-block-3035d81c">
          <div class="wp-block-uagb-image uagb-block-3fe0b2c7 wp-block-uagb-image--layout-default wp-block-uagb-image--effect-static wp-block-uagb-image--align-none"> 
            <div class="content-wrapper" style="margin-left: 0px;">
              <div class="content-wrapper" style="margin-left: 0px;">
                <div class="content-wrapper" style="margin-left: 0px;">
                  <section class="content-header">
                    <div class="container">
                      <div class="row latest-listing margin-top-50 margin-bottom-20 bg-white">
                        <h2 class="text-center margin-bottom-20">CREATE TRAVELER PROFILE</h2>
                          <form action="addtraveler.php" enctype="multipart/form-data" method="POST" id="registerTraveler"  >
                            <div class="col-md-6 latest-listing ">
                              <div class="form-group">
                                <input class="form-control input-lg" type="text" id="fname" name="fname" placeholder="First Name *" required>
                              </div>
                              <div class="form-group">
                                <input class="form-control input-lg" type="text" id="lname" name="lname" placeholder="Last Name *" required>
                              </div>
                              <div class="form-group">
                                <input class="form-control input-lg" type="email" id="email" name="email" placeholder="Email *" required>
                              </div>
                              <div class="form-group">
                                <textarea class="form-control input-lg" rows="4" id="aboutme" name="aboutme" placeholder="Brief intro about yourself *" required></textarea>
                              </div>
                              <div class="form-group">
                                <label>Date Of Birth</label>
                                <input class="form-control input-lg" type="date" id="dob" min="1960-01-01" max="2005-01-31" name="dob" placeholder="Date Of Birth">
                              </div>  
                              <div class="form-group">
                                <label>Attach Profile Picture</label>
                                <input type="file" name="traveler_picture" id="traveler_picture" class="form-control input-lg" required accept="image/*">
                              </div>   
                              <div class="form-group checkbox">
                                <label><input type="checkbox" checked required> I accept terms & conditions</label>
                              </div>
                              <div class="form-group">
                                <button class="btn btn-flat btn-success">Register</button>
                              </div>
                              <?php 
                              //If User already registered with this email then show error message.
                              if(isset($_SESSION['registerError'])) {
                                ?>
                                <div class="form-group">
                                  <label style="color: red;">Email Already Exists! Choose A Different Email!</label>
                                </div>
                              <?php
                                unset($_SESSION['registerError']); }
                              ?> 

                              <?php if(isset($_SESSION['uploadError'])) { ?>
                              <div class="form-group">
                                <label style="color: red;"><?php echo $_SESSION['uploadError']; ?></label>
                              </div>
                              <?php unset($_SESSION['uploadError']); } ?>   
                            </div>            
                            <div class="col-md-6 latest-listing ">
                              <div class="form-group">
                                <input class="form-control input-lg" type="password" id="password" name="password" placeholder="Password *" required>
                              </div>
                              <div class="form-group">
                                <input class="form-control input-lg" type="password" id="cpassword" name="cpassword" placeholder="Confirm Password *" required>
                              </div>
                              <div id="passwordError" class="btn btn-flat btn-danger hide-me" >
                                Password Mismatch!! 
                              </div>
                              <div class="form-group">
                                <input class="form-control input-lg" type="text" id="contactno" name="contactno" minlength="10" maxlength="14" onkeypress="return validatePhone(event);" placeholder="Phone Number">
                              </div>
                              <div class="form-group">
                                <textarea class="form-control input-lg" rows="4" id="address" name="address" placeholder="Address"></textarea>
                              </div>
                              <div class="form-group">
                                <input class="form-control input-lg" type="text" id="city" name="city" placeholder="City">
                              </div>
                              <div class="form-group">
                                <input class="form-control input-lg" type="text" id="state" name="state" placeholder="State" required>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.content-wrapper -->
      <?php include('footer.php'); ?>
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
      <div class="control-sidebar-bg">
      </div>
    </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="js/adminlte.min.js"></script>
  <script type="text/javascript">
    function validatePhone(event) 
    {
      //event.keycode will return unicode for characters and numbers like a, b, c, 5 etc.
      //event.which will return key for mouse events and other events like ctrl alt etc. 
      var key = window.event ? event.keyCode : event.which;

      if(event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39) 
      {
        // 8 means Backspace
        //46 means Delete
        // 37 means left arrow
        // 39 means right arrow
        return true;
      } 
      else if( key < 48 || key > 57 ) 
      {
        // 48-57 is 0-9 numbers on your keyboard.
        return false;
      } 
      else return true;
    }
  </script>

  <script type="text/javascript">
    $('#dob').on('change', function() {
      var today = new Date();
      var birthDate = new Date($(this).val());
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();

      if(m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }

      // $('#age').val(age);
    });
  </script>
  <script>
    $("#registerTraveler").on("submit", function(e) {
      e.preventDefault();
      if( $('#password').val() != $('#cpassword').val() ) 
      {
        $('#passwordError').show();
      } 
      else 
      {
        $(this).unbind('submit').submit();
      }
    });
  </script>
</body>
</html>
