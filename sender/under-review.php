<?php

//To Handle Session Variables on This Page
session_start();

//If user Not logged in then redirect them back to homepage. 
//This is required if user tries to manually enter view-listing.php in URL.
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

//Including Database Connection From db.php file to avoid rewriting in all files  
require_once("../db.php");

$sql = "SELECT * FROM listing_response WHERE id_sender='$_SESSION[id_sender]' AND id_traveler='$_GET[id]' AND id_listing='$_GET[id_listing]'";
$result = $conn->query($sql);
if($result->num_rows == 0) 
{
  header("Location: index.php");
  exit();
}


$sql = "UPDATE listing_response SET status='2' WHERE id_sender='$_SESSION[id_sender]' AND id_traveler='$_GET[id]' AND id_listing='$_GET[id_listing]'";
if($conn->query($sql) === TRUE) {
	header("Location: traveler-response.php");
	exit();
}

?>