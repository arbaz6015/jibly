<?php

//To Handle Session Variables on This Page
session_start();

//If user Not logged in then redirect them back to homepage. 
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

require_once("../db.php");
?>
<!DOCTYPE html>
<html>

<!-- <body class="hold-transition skin-green sidebar-mini"> -->
  <!-- <div class="wrapper"> -->
    <?php include('header.php'); ?>
    <!-- Content Wrapper. Contains page content -->
          <div class="wp-block-uagb-container uagb-block-79e82c98 alignfull uagb-is-root-container">
            <div class="uagb-container-inner-blocks-wrap">
             <div class="wp-block-uagb-container uagb-block-3035d81c">
              <div class="wp-block-uagb-image uagb-block-3fe0b2c7 wp-block-uagb-image--layout-default wp-block-uagb-image--effect-static wp-block-uagb-image--align-none"> 
                <div class="content-wrapper" style="margin-left: 0px;">
                    <section id="candidates" class="content-header">
                      <div class="container">
                        <div class="row">
                        <?php include('sidebar.php'); ?>
                          <div class="col-md-9 bg-white padding-2">
                            <h3>Overview</h3>
                            <div class="alert alert-info alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="icon fa fa-info"></i> In this dashboard you are able to change your account settings, and manage your listings. Got a question? Do not hesitate to drop us a mail.
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="info-box bg-c-yellow">
                                  <span class="info-box-icon bg-red"><i class="fa fa-cubes"></i></span>
                                  <a href="my-listing.php">
                                    <div class="info-box-content">
                                      <span class="info-box-text">Item Listed</span>
                                      <?php
                                        $sql = "SELECT * FROM `listing` WHERE `id_sender`='$_SESSION[id_sender]'";
                                        $result = $conn->query($sql);

                                        if($result->num_rows > 0) {
                                          $total = $result->num_rows; 
                                        } else {
                                          $total = 0;
                                        }
                                      ?>
                                      <span class="info-box-number"><?php echo $total; ?></span>
                                    </div>
                                  </a>
                                </div>                
                              </div>
                              <div class="col-md-6">
                                <div class="info-box bg-c-yellow">
                                  <span class="info-box-icon bg-green"><i class="fa fa-paper-plane"></i></span>
                                  <a href="traveler-response.php">
                                    <div class="info-box-content">
                                      <span class="info-box-text">Traveler Response</span>
                                      <?php
                                      $sql = "SELECT * FROM listing_response WHERE id_sender='$_SESSION[id_sender]'";
                                      $result = $conn->query($sql);

                                      if($result->num_rows > 0) {
                                        $total = $result->num_rows; 
                                      } else {
                                        $total = 0;
                                      }
                                    ?>
                                      <span class="info-box-number">
                                        <?php echo $total; ?>
                                      </span>
                                    </div>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
             </div>
          </div>
        </div>
    <!-- /.content-wrapper -->
  <?php include('footer.php'); ?>
  </div>
  <!-- ./wrapper -->
  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../js/adminlte.min.js"></script>
</body>
</html>

