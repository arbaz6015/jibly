<?php

//To Handle Session Variables on This Page
session_start();

//If user Not logged in then redirect them back to homepage. 
//This is required if user tries to manually enter view-listing.php in URL.
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

//Including Database Connection From db.php file to avoid rewriting in all files  
require_once("../db.php");

$sql = "SELECT * FROM listing_response WHERE id_sender='$_SESSION[id_sender]' AND id_traveler='$_GET[id]'";
$result = $conn->query($sql);
if($result->num_rows == 0) 
{
  header("Location: index.php");
  exit();
}
?>
<!DOCTYPE html>
<html>
<?php include('header-top.php'); ?>
<!-- <body class="hold-transition skin-green sidebar-mini">
<div class="wrapper"> -->

  <?php include('header.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin-left: 0px;">

    <section id="candidates" class="content-header">
      <div class="container">
        <div class="row">
        <?php include('sidebar.php'); ?>
          <div class="col-md-9 bg-white padding-2">
            <div class="row margin-top-20">
              <div class="col-md-12">
              <?php
               $sql = "SELECT * FROM traveler WHERE id_traveler='$_GET[id]'";
                $result = $conn->query($sql);

                //If Item Listing exists then display details of post
                if($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) 
                  {
                ?>
                <div class="pull-left">
                  <h2><b><i><?php echo $row['firstname']. ' '.$row['lastname']; ?></i></b></h2>
                </div>
                <div class="pull-right">
                  <a href="traveler-response.php" class="btn btn-default btn-lg btn-flat margin-top-20"><i class="fa fa-arrow-circle-left"></i> Back</a>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div>
                  <?php
                    echo 'Email: '.$row['email'];
                    echo '<br>';
                    echo 'City: '.$row['city'];
                    echo '<br>';
                    if($row['resume'] != "") {
                      echo '<a href="../uploads/resume/'.$row['resume'].'" class="btn btn-info" download="Resume">Download Resume</a>';
                    }
                    echo '<br>';
                    echo '<br>';
                    echo '<br>';
                    echo '<br>';
                  ?>
                  <div class="row">
                    <div class="col-md-3 pull-left">
                      <a href="under-review.php?id=<?php echo $row['id_traveler']; ?>&id_listing=<?php echo $_GET['id_listing']; ?>" class="btn btn-success">Mark Under Review</a>
                    </div>
                    <div class="col-md-3 pull-right">
                      <a href="reject.php?id=<?php echo $row['id_traveler']; ?>&id_listing=<?php echo $_GET['id_listing']; ?>" class="btn btn-danger">Reject Application</a>
                    </div>
                  </div>
                </div>

                <div>
                </div>
                <?php
                  }
                }
                ?>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>


    

  </div>
  <!-- /.content-wrapper -->

<?php include('footer.php'); ?>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<!-- AdminLTE App -->
<script src="../js/adminlte.min.js"></script>

</body>
</html>
