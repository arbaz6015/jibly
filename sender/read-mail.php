
<?php

//To Handle Session Variables on This Page
session_start();

//If user Not logged in then redirect them back to homepage. 
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}
require_once("../db.php");
$sql = "SELECT * FROM mailbox WHERE id_mailbox='$_GET[id_mail]' AND (id_fromuser='$_SESSION[id_sender]' OR id_touser='$_SESSION[id_sender]')";
$result = $conn->query($sql);
if($result->num_rows >  0 ){
  $row = $result->fetch_assoc();
  if($row['usertype'] == "sender") {
    $sql1 = "SELECT * FROM sender WHERE id_sender='$row[id_fromuser]'";
    $result1 = $conn->query($sql1);
    if($result1->num_rows >  0 ){
      $rowCompany = $result1->fetch_assoc();
    }
    $sql2 = "SELECT * FROM traveler WHERE id_traveler='$row[id_touser]'";
    $result2 = $conn->query($sql2);
    if($result2->num_rows >  0 ){
      $rowUser = $result2->fetch_assoc();
    }
  } else {
    $sql1 = "SELECT * FROM sender WHERE id_sender='$row[id_touser]'";
    $result1 = $conn->query($sql1);
    if($result1->num_rows >  0 ){
      $rowCompany = $result1->fetch_assoc();
    }
    $sql2 = "SELECT * FROM traveler WHERE id_traveler='$row[id_fromuser]'";
    $result2 = $conn->query($sql2);
    if($result2->num_rows >  0 ){
      $rowUser = $result2->fetch_assoc();
    }
  }
}
?>
<!DOCTYPE html>
<html>
<?php include('header-top.php'); ?>
<!-- <body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper"> -->
    <?php include('header.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-left: 0px;">
      <section id="candidates" class="content-header">
        <div class="container">
          <div class="row">
          <?php include('sidebar.php'); ?>
            <div class="col-md-9 bg-white padding-2">
            <section class="content">
              <div class="row">
                <div class="col-md-12">
                  <a href="mailbox.php" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> Back</a>
                  <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                      <div class="mailbox-read-info">
                        <h3><?php echo $row['subject']; ?></h3>
                        <h5>From: <?php echo $rowCompany['sender_agency']; ?>
                          <span class="mailbox-read-time pull-right"><?php echo date("d-M-Y h:i a", strtotime($row['createdAt'])); ?></span></h5>
                      </div>
                      <div class="mailbox-read-message">
                        <?php echo stripcslashes($row['message']); ?>
                      </div>
                      <!-- /.mailbox-read-message -->
                    </div>
                    <!-- /.box-body -->
                  </div>

                  <?php

                  $sqlReply = "SELECT * FROM reply_mailbox WHERE id_mailbox='$_GET[id_mail]'";
                  $resultReply = $conn->query($sqlReply);
                  if($resultReply->num_rows > 0) {
                    while($rowReply =  $resultReply->fetch_assoc()) {
                      ?>
                    <div class="box box-primary">
                      <div class="box-body no-padding">
                        <div class="mailbox-read-info">
                          <h3>Reply Message</h3>
                          <h5>From: <?php if($rowReply['usertype'] == "sender") { echo $rowCompany['sender_agency']; } else { echo $rowUser['firstname']; } ?>
                            <span class="mailbox-read-time pull-right"><?php echo date("d-M-Y h:i a", strtotime($rowReply['createdAt'])); ?></span></h5>
                        </div>
                        <div class="mailbox-read-message">
                          <?php echo stripcslashes($rowReply['message']); ?>
                        </div>
                      </div>
                    </div>
                      <?php
                    }
                  }
                  ?>
                  
                  <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                      <div class="mailbox-read-info">
                        <h3>Send Reply</h3>
                      </div>
                      <div class="mailbox-read-message">
                        <form action="reply-mailbox.php" method="post">
                          <div class="form-group">
                            <textarea class="form-control input-lg" id="description" name="description" placeholder="Item Description"></textarea>
                            <input type="hidden" name="id_mail" value="<?php echo $_GET['id_mail']; ?>">
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-flat btn-success">Reply</button>
                          </div>
                        </form>
                      </div>
                      <!-- /.mailbox-read-message -->
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </section>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->
    <?php include('footer.php'); ?>

  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../js/adminlte.min.js"></script>
  <!-- DataTables -->
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script>
    $(function () {
      $('#example1').DataTable();
    })
  </script>
</body>
</html>
