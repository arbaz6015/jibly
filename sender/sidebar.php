<?php
    $curr_url = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
    $curr_url = preg_replace('/\\.[^.\\s]{3,4}$/', '', $curr_url);
?>
<div class="col-md-3">
    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Welcome <b><?php echo $_SESSION['name']; ?></b></h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="<?= ($curr_url=='index')?'active':'';?>"><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="<?= ($curr_url=='edit-sender')?'active':'';?>"><a href="edit-sender.php"><i class="fa fa-user"></i> My Profile</a></li>
                <li class="<?= ($curr_url=='create-listing')?'active':'';?>"><a href="create-listing.php"><i class="fa fa-cube"></i> List Item</a></li>
                <li class="<?= ($curr_url=='my-listing')?'active':'';?>"><a href="my-listing.php"><i class="fa fa-cubes"></i> My Listing</a></li>
                <li class="<?= ($curr_url=='traveler-response')?'active':'';?>"><a href="traveler-response.php"><i class="fa fa-paper-plane"></i> Traveler Response</a></li>
                <li class="<?= ($curr_url=='mailbox')?'active':'';?>"><a href="mailbox.php"><i class="fa fa-envelope"></i> Mailbox</a></li>
                <li class="<?= ($curr_url=='settings')?'active':'';?>"><a href="settings.php"><i class="fa fa-gear"></i> Settings</a></li>
                <!-- <li class="<?= ($curr_url=='traveler-database')?'active':'';?>"><a href="traveler-database.php"><i class="fa fa-user"></i> Traveler Database</a></li> -->
                <li class="<?= ($curr_url=='logout')?'active':'';?>"><a href="../logout.php"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
        </div>
    </div>
</div>