<?php

//To Handle Session Variables on This Page
session_start();

//If user Not logged in then redirect them back to homepage. 
//This is required if user tries to manually enter view-listing.php in URL.
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}
require_once("../db.php");
?>
<!DOCTYPE html>
<html>
<?php include('header-top.php'); ?>
<!-- <body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper"> -->
    <?php include('header.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-left: 0px;">
      <section id="candidates" class="content-header">
        <div class="container">
          <div class="row">
          <?php include('sidebar.php'); ?>
            <div class="col-md-9 bg-white padding-2">
              <h2><i>Traveler Database</i></h2>
              <p>In this section you can download resume of all candidates who applied to your item listings</p>
              <div class="row margin-top-20">
                <div class="col-md-12">
                  <div class="box-body table-responsive no-padding">
                    <table id="example2" class="table table-hover">
                      <thead>
                        <th>Candidate</th>
                        <th>Highest Qualification</th>
                        <th>Skills</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Download Resume</th>
                      </thead>
                      <tbody>
                      <?php
                        $sql = "SELECT traveler.* FROM listing INNER JOIN listing_response ON listing.id_listing=listing_response.id_listing  INNER JOIN traveler ON traveler.id_traveler=listing_response.id_traveler WHERE listing_response.id_sender='$_SESSION[id_sender]' GROUP BY traveler.id_traveler";
                              $result = $conn->query($sql);

                              if($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) 
                                {     

                                  $skills = $row['skills'];
                                  $skills = explode(',', $skills);
                        ?>
                        <tr>
                          <td><?php echo $row['firstname'].' '.$row['lastname']; ?></td>
                          <td><?php echo $row['item_picture']; ?></td>
                          <td>
                            <?php
                            foreach ($skills as $value) {
                              echo ' <span class="label label-success">'.$value.'</span>';
                            }
                            ?>
                          </td>
                          <td><?php echo $row['city']; ?></td>
                          <td><?php echo $row['state']; ?></td>
                          <td><a href="../uploads/resume/<?php echo $row['resume']; ?>" download="<?php echo $row['firstname'].' Resume'; ?>"><i class="fa fa-file-pdf-o"></i></a></td>
                        </tr>
                        <?php

                          }
                        }
                        ?>
                      </tbody>                    
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->

    <?php include('footer.php'); ?>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- DataTables -->
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../js/adminlte.min.js"></script>


  <script>
    $(function () {
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      });
    });
  </script>
</body>
</html>
