<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Jibly.io | Sender </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css/AdminLTE.min.css">
  <link rel="stylesheet" href="../css/_all-skins.min.css">
  <!-- Custom -->
  <link rel="stylesheet" href="../css/custom.css?ver=<?= DATE('Y-m-d H:i:s'); ?>">

  <script src="../js/tinymce/tinymce.min.js"></script>

  <script>tinymce.init({ selector:'#description', height: 300 });</script>

  <script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php
  session_start();
  if(isset($_SESSION['uploadError']))
  {
    echo "<script>$(document).ready(function() {toastr.error('".$_SESSION['uploadError']."');});
    </script>";
    unset($_SESSION['uploadError']); 
  }
  if(isset($_SESSION['uploadSuccess']))
  {
    echo "<script>$(document).ready(function() {toastr.success('".$_SESSION['uploadSuccess']."');});
    </script>";
    unset($_SESSION['uploadSuccess']); 
  }
  if(isset($_SESSION['itemListingSuccess']))
  {
    echo "<script>$(document).ready(function() {toastr.success('".$_SESSION['itemListingSuccess']."');});
    </script>";
    unset($_SESSION['itemListingSuccess']); 
  }
  if(isset($_SESSION['itemActionSuccess']))
  {
    echo "<script>$(document).ready(function() {toastr.success('".$_SESSION['itemActionSuccess']."');});
    </script>";
    unset($_SESSION['itemActionSuccess']); 
  }
  if(isset($_SESSION['itemActionFail']))
  {
    echo "<script>$(document).ready(function() {toastr.error('".$_SESSION['itemActionFail']."');});
    </script>";
    unset($_SESSION['itemActionFail']); 
  }

?>