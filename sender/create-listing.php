<?php

//To Handle Session Variables on This Page
session_start();

//If user Not logged in then redirect them back to homepage. 
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

require_once("../db.php");
?>
<!DOCTYPE html>
<html>

<!-- <body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper"> -->
    <?php include('header.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="wp-block-uagb-container uagb-block-79e82c98 alignfull uagb-is-root-container">
  <div class="uagb-container-inner-blocks-wrap">
<div class="wp-block-uagb-container uagb-block-3035d81c">
<div class="wp-block-uagb-image uagb-block-3fe0b2c7 wp-block-uagb-image--layout-default wp-block-uagb-image--effect-static wp-block-uagb-image--align-none"> 
  <div class="content-wrapper" style="margin-left: 0px;">
    <div class="content-wrapper" style="margin-left: 0px;">
      <section id="candidates" class="content-header">
        <div class="container">
          <div class="row">
          <?php include('sidebar.php'); ?>
            <div class="col-md-9 bg-white padding-2">
              <h2><i>List item</i></h2>
              <div class="row">
                <form method="post" id="add_listing" name="add_listing" action="addlisting.php" enctype="multipart/form-data">
                  <div class="col-md-12 latest-listing">
                    <div class="form-group">
                      <input class="form-control input-lg" type="text" id="listing_title" name="listing_title" placeholder="Item Title" required>
                    </div>
                    <div class="form-group">
                      <textarea class="form-control input-lg" id="description" name="description" placeholder="Item Description"></textarea>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="pickup_address">Pickup Address</label>
                          <textarea class="form-control input-lg ml-0 pl-0" id="pickup_address" name="pickup_address" placeholder="Pickup Address" required="required"></textarea>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="dropoff_address">Dropoff Address</label>
                          <textarea class="form-control input-lg" id="dropoff_address" name="dropoff_address"  placeholder="Dropoff Address" required="required"></textarea>
                        </div>
                      </div>
                    </div>                  
                    <div class="form-group">
                      <label for="item_weight">Item weight in lbs</label>
                      <input type="number" class="form-control input-lg" id="item_weight" name="item_weight" placeholder="Item weight in lbs" required="required">
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Deliver By</label>
                          <input type="datetime-local" class="form-control input-lg" id="deliver_by" name="deliver_by" placeholder="Enter required deivery date and time" required="required">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Item Picture</label>
                          <input type="file" enctype="multipart/form-data" accept="image/png, image/gif, image/jpeg" class="form-control input-lg" id="item_picture" name="item_picture[]" placeholder="Item Picture" multiple>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-lg btn-flat btn-success">Add</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div></div></div></div></div>
    <!-- /.content-wrapper -->

    
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    
  </div>
  <?php include('footer.php'); ?>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../js/adminlte.min.js"></script>
  <script>
    $(document).ready(function(e){
      $(document).on("submit","#add_listing",function(e2){
        e.preventDefault();
        if($("#description".val().length() < 20)){
          toastr.error('Please enter a brief description or instruction (if any) about your item');
          return false;
        }
      });
    });
  </script>
</body>
</html>
