<?php

//To Handle Session Variables on This Page
session_start();

if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

//Including Database Connection From db.php file to avoid rewriting in all files
require_once("../db.php");

//if user Actually clicked Add Post Button
if(isset($_POST)) {

	// New way using prepared statements. This is safe from SQL INJECTION. Should consider to update to this method when many people are using this method.

	$stmt = $conn->prepare("INSERT INTO listing(`id_sender`, `encrypted_id`,`listing_title`, `description`, `pickup_address`, `dropoff_address`, `item_weight`, `item_picture`,`deliver_by`) VALUES (?,?,?,?,?,?,?,?,?);");

	$encrypted_id = md5($_SESSION['id_sender']);
	$listing_title = trim(mysqli_real_escape_string($conn, $_POST['listing_title']));
	$description = trim(mysqli_real_escape_string($conn, $_POST['description']));
	if(empty($description)){
		$_SESSION['uploadError'] = "Please enter a brief description or instruction (if any) about your item";
		header("Location: create-listing.php");
		exit();
	}
	$pickup_address = trim(mysqli_real_escape_string($conn, $_POST['pickup_address']));
	$dropoff_address = trim(mysqli_real_escape_string($conn, $_POST['dropoff_address']));
	$item_weight = mysqli_real_escape_string($conn, $_POST['item_weight']);
	$deliver_by = mysqli_real_escape_string($conn, $_POST['deliver_by']);
	
	// echo '<pre>';
	// print_r($_FILES);
	// exit;

	if(isset($_FILES['item_picture']) && !empty($_FILES['item_picture']['name'][0])){
		$uploadOk = true;	
		$folder_dir = "../uploads/item/";
		$abs_folder_dir = "uploads/item/";
		$images = '';
		foreach($_FILES['item_picture']['name'] as $key=>$val)
		{
			$base = basename($val); 
			$imageFileType = pathinfo($base, PATHINFO_EXTENSION); 
		
			$file = uniqid() . "." . $imageFileType; 
			
			$filename = $folder_dir .$file;  
			$abs_filename = $abs_folder_dir .$file;  
		
			//We check if file is saved to our temp location or not.
			if(file_exists($_FILES['item_picture']['tmp_name'][$key])) 
			{ 
		
				if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg")  {
		
					if($_FILES['item_picture']['size'][$key] < 500000) 
					{ 	
						move_uploaded_file($_FILES["item_picture"]["tmp_name"][$key], $filename);
						// print_r($filename); exit;
					} 
					else {
						//Size Error
						$_SESSION['uploadError'] = "Wrong Size. Max Size Allowed : 5MB";
						$uploadOk = false;
					}
				} else {
					//Format Error
					$_SESSION['uploadError'] = "Wrong Format. Only jpg & png Allowed";
					$uploadOk = false;
				}
			} 
			else 
			{
				//File not copied to temp location error.
				$_SESSION['uploadError'] = "Something Went Wrong. Item images not uploaded. Please try Again.";
				$uploadOk = false;
			}
		
			//If there is any error then redirect back.
			if($uploadOk == false) {
				header("Location: create-listing.php");
				exit();
			}
			$images = $images.$abs_filename.',';
		}
		$images = rtrim($images, ",");
		$item_picture = mysqli_real_escape_string($conn, $images);
	}
	else{
		$item_picture = mysqli_real_escape_string($conn, 'img/item.jpg');
	}

	// print_r($_POST); exit;
	$stmt->bind_param("sssssssss",$_SESSION['id_sender'], $encrypted_id, $listing_title, $description, $pickup_address, $dropoff_address, $item_weight, $item_picture, $deliver_by);

	if($stmt->execute()) {
		//If data Inserted successfully then redirect to dashboard
		$_SESSION['itemListingSuccess'] = 'Successfully listed your item for delivery';
		header("Location: my-listing.php");
		exit();
	} else {
		//If data failed to insert then show that error. Note: This condition should not come unless we as a developer make mistake or someone tries to hack their way in and mess up :D
		$_SESSION['uploadError'] = 'Something went wrong';
		header("Location: create-listing.php");
		exit();
	}

	$stmt->close();

	//THIS IS NOT SAFE FROM SQL INJECTION BUT OK TO USE WITH SMALL TO MEDIUM SIZE AUDIENCE

	//Insert Item Listing Query 
	// $sql = "INSERT INTO listing(id_sender, listing_title, description, pickup_address, dropoff_address, item_weight, item_picture) VALUES ('$_SESSION[id_sender]','$listing_title', '$description', '$pickup_address', '$dropoff_address', '$item_weight', '$item_picture')";

	// if($conn->query($sql)===TRUE) {
	// 	//If data Inserted successfully then redirect to dashboard
	// 	$_SESSION['itemListingSuccess'] = true;
	// 	header("Location: dashboard.php");
	// 	exit();
	// } else {
	// 	//If data failed to insert then show that error. Note: This condition should not come unless we as a developer make mistake or someone tries to hack their way in and mess up :D
	// 	echo "Error " . $sql . "<br>" . $conn->error;
	// }

	//Close database connection. Not compulsory but good practice.
	$conn->close();

} 
else {
	//redirect them back to dashboard page if they didn't click Add Post button
	header("Location: create-listing.php");
	exit();
}