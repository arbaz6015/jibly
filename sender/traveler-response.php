<?php
session_start();

//If user Not logged in then redirect them back to homepage. 
//This is required if user tries to manually enter view-listing.php in URL.
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

//To Handle Session Variables on This Page


//If user Not logged in then redirect them back to homepage. 

require_once("../db.php");
?>
<!DOCTYPE html>
<html>

<!-- <body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper"> -->
    <?php include('header.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="wp-block-uagb-container uagb-block-79e82c98 alignfull uagb-is-root-container">
  <div class="uagb-container-inner-blocks-wrap">
<div class="wp-block-uagb-container uagb-block-3035d81c">
<div class="wp-block-uagb-image uagb-block-3fe0b2c7 wp-block-uagb-image--layout-default wp-block-uagb-image--effect-static wp-block-uagb-image--align-none"> 
  <div class="content-wrapper" style="margin-left: 0px;">
    <div class="content-wrapper" style="margin-left: 0px;">
      <section id="candidates" class="content-header">
        <div class="container">
          <div class="row">
          <?php include('sidebar.php'); ?>
            <div class="col-md-9 bg-white padding-2">
              <h2><i>Recent Response</i></h2>
              <?php
                $sql = "SELECT * FROM `listing` INNER JOIN `listing_response` ON listing.id_listing=listing_response.id_listing  INNER JOIN `traveler` ON traveler.id_traveler=listing_response.id_traveler WHERE listing_response.id_sender='$_SESSION[id_sender]'";
                $result = $conn->query($sql);
                if($result->num_rows > 0) 
                {
                  while($row = $result->fetch_assoc()) 
                  {     
              ?>
              <div class="attachment-block clearfix padding-2">
                  <h4 class="attachment-heading"><a href="view-traveler-response.php?id=<?php echo $row['id_traveler']; ?>&id_listing=<?php echo $row['id_listing']; ?>"><?php echo $row['listing_title'].' @ ('.$row['firstname'].' '.$row['lastname'].')'; ?></a></h4>
                  <div class="attachment-text padding-2">
                    <div class="pull-left"><i class="fa fa-calendar"></i> <?php echo $row['createdat']; ?></div>  
                    <?php 

                    if($row['status'] == 0) {
                      echo '<div class="pull-right"><strong class="text-orange">Pending</strong></div>';
                    } else if ($row['status'] == 1) {
                      echo '<div class="pull-right"><strong class="text-red">Rejected</strong></div>';
                    } else if ($row['status'] == 2) {
                      echo '<div class="pull-right"><strong class="text-green">Under Review</strong></div> ';
                    }
                    ?>         
                  </div>
              </div>
              <?php
                }
              }
              ?>
            </div>
        </div>
      </section>
    </div>
  </div></div></div></div></div>
    <!-- /.content-wrapper -->
  <?php include('footer.php'); ?>

  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../js/adminlte.min.js"></script>
</body>
</html>
