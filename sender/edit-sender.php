<?php

//To Handle Session Variables on This Page
session_start();

//If user Not logged in then redirect them back to homepage. 
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

require_once("../db.php");
?>
<!DOCTYPE html>
<html>
<?php include('header-top.php'); ?>
<!-- <body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper"> -->
    <?php include('header.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="wp-block-uagb-container uagb-block-79e82c98 alignfull uagb-is-root-container">
  <div class="uagb-container-inner-blocks-wrap">
<div class="wp-block-uagb-container uagb-block-3035d81c">
<div class="wp-block-uagb-image uagb-block-3fe0b2c7 wp-block-uagb-image--layout-default wp-block-uagb-image--effect-static wp-block-uagb-image--align-none"> 
  <div class="content-wrapper" style="margin-left: 0px;">
    <div class="content-wrapper" style="margin-left: 0px;">
      <section id="candidates" class="content-header">
        <div class="container">
          <div class="row">
            <?php include('sidebar.php'); ?>
            <div class="col-md-9 bg-white padding-2">
              <h2><i>My Profile</i></h2>
              <p>In this section you can change sender details</p>
              <div class="row">
                <form action="update-sender.php" method="post" enctype="multipart/form-data">
                  <?php
                  $sql = "SELECT * FROM sender WHERE id_sender='$_SESSION[id_sender]'";
                  $result = $conn->query($sql);

                  if($result->num_rows > 0) 
                  {
                    while($row = $result->fetch_assoc()) 
                    {
                  ?>
                    <div class="col-md-12 latest-listing "> 
                      <img src="../uploads/logo/<?= $row['profile_picture']; ?>" class="avatar">
                    </div>
                    <div class="col-md-6 latest-listing ">   
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control input-lg" name="name" value="<?php echo $row['name']; ?>" required="required">
                    </div>
                    
                    <div class="form-group">
                      <label for="email">Email address</label>
                      <input type="email" class="form-control input-lg" id="email" placeholder="Email" value="<?php echo $row['email']; ?>" readonly>
                    </div>
                    
                    <div class="form-group">
                      <label>About Me</label>
                      <textarea class="form-control input-lg" rows="4" name="aboutme"><?php echo $row['aboutme']; ?></textarea>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-flat btn-success">Update Profile</button>
                    </div>
                  </div>
                  <div class="col-md-6 latest-listing ">
                    <div class="form-group">
                      <label for="contactno">Contact Number</label>
                      <input type="text" class="form-control input-lg" id="contactno" name="contactno" placeholder="Contact Number" onkeypress="return validatePhone(event);" minlength="10" maxlength="10" value="<?php echo $row['contactno']; ?>">
                    </div>
                    <div class="form-group">
                      <label for="city">City</label>
                      <input type="text" class="form-control input-lg" id="city" name="city"
                      onkeypress="return validateName(event);" value="<?php echo $row['city']; ?>" placeholder="city">
                    </div>
                    <div class="form-group">
                      <label for="state">State</label>
                      <input type="text" class="form-control input-lg" id="state" onkeypress="return validateName(event);" name="state" placeholder="state" value="<?php echo $row['state']; ?>">
                    </div>
                    <div class="form-group">
                      <label>Change Profile Picture</label>
                      <input type="file" name="image" class="form-control input-lg" accept="image/png, image/gif, image/jpeg">
                      <?php if($row['logo'] != "") { ?>
                      <img src="../uploads/logo/<?php echo $row['logo']; ?>" class="img-responsive" style="max-height: 200px; max-width: 200px;">
                      <?php } ?>
                    </div>
                  </div>
                      <?php
                      }
                    }
                  ?>  
                </form>
              </div>
              <?php if(isset($_SESSION['uploadError'])) { ?>
              <div class="row">
                <div class="col-md-12 text-center">
                  <?php echo $_SESSION['uploadError']; ?>
                </div>
              </div>
              <?php unset($_SESSION['uploadError']); } ?>
              
            </div>
          </div>
        </div>
      </section>
    </div>
  </div></div></div></div></div>
    <!-- /.content-wrapper -->
  <?php include('footer.php'); ?>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../js/adminlte.min.js"></script>
  <script type="text/javascript">
    function validatePhone(event) {
      //event.keycode will return unicode for characters and numbers like a, b, c, 5 etc.
      //event.which will return key for mouse events and other events like ctrl alt etc. 
      var key = window.event ? event.keyCode : event.which;

      if(event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39) {
        // 8 means Backspace
        //46 means Delete
        // 37 means left arrow
        // 39 means right arrow
        return true;
      } else if( key < 48 || key > 57 ) {
        // 48-57 is 0-9 numbers on your keyboard.
        return false;
      } else return true;
    }

      function validateName(event) {

      //event.keycode will return unicode for characters and numbers like a, b, c, 5 etc.
      //event.which will return key for mouse events and other events like ctrl alt etc. 
      var key = window.event ? event.keyCode : event.which;

      if(event.keyCode == 8 || event.keyCode == 127 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 32) {
      
        return true;
      } else if( key < 65 || key > 90 && key < 97 || key > 122) {
        // 65-90 97-122 is A-Z a-z alphabets on your keyboard.
        return false;
      } else return true;
    }
  </script>
</body>
</html>
