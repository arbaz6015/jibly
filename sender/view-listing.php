<?php

//To Handle Session Variables on This Page
session_start();

//If user Not logged in then redirect them back to homepage. 
//This is required if user tries to manually enter view-listing.php in URL.
if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

//Including Database Connection From db.php file to avoid rewriting in all files  
require_once("../db.php");
?>
<!DOCTYPE html>
<html>
<?php include('header-top.php'); ?>
<!-- <body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper"> -->
    <?php include('header.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-left: 0px;">
      <section id="candidates" class="content-header">
        <div class="container">
          <div class="row">
          <?php include('sidebar.php'); ?>
            <div class="col-md-9 bg-white padding-2">
              <div class="row margin-top-20">
                <div class="col-md-12">
                <?php
                $sql = "SELECT * FROM listing WHERE id_sender='$_SESSION[id_sender]' AND id_listing='$_GET[id]'";
                  $result = $conn->query($sql);

                  //If Item Listing exists then display details of post
                  if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) 
                    {
                  ?>
                  <div class="pull-left">
                    <h2><b><i><?php echo $row['listing_title']; ?></i></b></h2>
                  </div>
                  <div class="pull-right">
                    <a href="my-listing.php" class="btn btn-default btn-lg btn-flat margin-top-20"><i class="fa fa-arrow-circle-left"></i> Back</a>
                  </div>
                  <div class="clearfix"></div>
                  <hr>
                  <div>
                    <p>
                      <span class="margin-right-10">
                        <i class="fa fa-balance-scale text-green">
                        </i> 
                        Item weight: <?php echo $row['item_weight']; ?> lbs
                      </span> 
                      <span class="margin-right-10">
                        <i class="fa fa-calendar text-green"></i> Listed on: <?php echo date("d-M-Y H:i:s", strtotime($row['createdat'])); ?>
                      </span> 
                      <span class="margin-right-10">
                        <i class="fa fa-calendar text-green"></i> Deliver by: <?php echo date("d-M-Y H:i:s", strtotime($row['deliver_by'])); ?>
                      </span> 
                    </p>              
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <?php 
                        $item_picture = $row['item_picture'];
                        if(!empty($item_picture)){
                          $item_picture = explode(",",$item_picture);
                          foreach($item_picture as $key=>$val){
                            echo '<img src="../'.$val.'" class="itemlist">';
                          }
                        }
                      ?>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-2">
                      <i class="fa fa-sticky-note-o text-green"></i> 
                      Description: 
                    </div>
                    <div class="col-md-10">
                      <?php echo stripcslashes($row['description']); ?>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-6">
                      <p><i class="fa fa-map-marker text-green"></i> Pickup Address: <?php echo stripcslashes($row['pickup_address']); ?></p>
                      <p><i class="fa fa-money text-green"></i> Fees: $25 USD</p>
                    </div>
                    <div class="col-md-6">
                      <p><i class="fa fa-map-marker text-green"></i> Drop off Address: <?php echo stripcslashes($row['dropoff_address']); ?></p>
                      <p><i class="fa fa-shopping-cart text-green"></i> Payment Status: <?php echo stripcslashes($row['payment_status']); ?></p>
                    </div>
                  </div>
                  <div>
                    <a href="#" class="btn btn-success btn-lg margin-top-20"><i class="fa fa-paypal"></i> Make Payment</a>
                  </div>
                  <?php
                    }
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->

  <?php include('footer.php'); ?>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- DataTables -->
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../js/adminlte.min.js"></script>
</body>
</html>
