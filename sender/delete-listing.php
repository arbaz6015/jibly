<?php
//To Handle Session Variables on This Page
session_start();

if(empty($_SESSION['id_sender'])) {
  header("Location: ../index.php");
  exit();
}

//Including Database Connection From db.php file to avoid rewriting in all files
require_once("../db.php");

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = $_GET['id'];
    $stmt = $conn->prepare("DELETE FROM `listing` WHERE `listing`.`id_listing` = '$id'");
    
    if($stmt->execute()) {
		$_SESSION['itemActionSuccess'] = 'Listing deleted successfully';
		header("Location: my-listing.php");
		exit();
	} 
    else {
		//If data failed to insert then show that error. Note: This condition should not come unless we as a developer make mistake or someone tries to hack their way in and mess up :D
		$_SESSION['itemActionFail'] = 'Invalid listing ID';
		header("Location: my-listing.php");
        exit();
	}
}