<?php

//To Handle Session Variables on This Page
session_start();

if(empty($_SESSION['id_traveler'])) {
	header("Location: index.php");
	exit();
}

//Including Database Connection From db.php file to avoid rewriting in all files
require_once("db.php");

//If user Actually clicked apply button
if(isset($_GET)) {

	$sql = "SELECT * FROM listing WHERE id_listing='$_GET[id]'";
	  $result = $conn->query($sql);
	  if($result->num_rows > 0) 
	  {
	    	$row = $result->fetch_assoc();
	    	$id_sender = $row['id_sender'];
	   }

	//Check if user has applied to job post or not. If not then add his details to listing_response table.
	$sql1 = "SELECT * FROM listing_response WHERE id_traveler='$_SESSION[id_traveler]' AND id_listing='$row[id_listing]'";
    $result1 = $conn->query($sql1);
    if($result1->num_rows == 0) {  
    	
    	$sql = "INSERT INTO listing_response(id_listing, id_sender, id_traveler) VALUES ('$_GET[id]', '$id_sender', '$_SESSION[id_traveler]')";

		if($conn->query($sql)===TRUE) {
			$_SESSION['jobApplySuccess'] = true;
			header("Location: user/index.php");
			exit();
		} else {
			echo "Error " . $sql . "<br>" . $conn->error;
		}

		$conn->close();

    }  else {
		header("Location: listing.php");
		exit();
	}
	

} else {
	header("Location: listing.php");
	exit();
}