
<?php
session_start();
if (isset($_SESSION['id_traveler']) || isset($_SESSION['id_sender'])) {
    header("Location: index.php");
    exit();
}
?>
                          <!DOCTYPE html>
                          <html>
                          <?php include 'header.php';?>
                          <div class="wp-block-uagb-container uagb-block-79e82c98 alignfull uagb-is-root-container">
                          <div class="uagb-container-inner-blocks-wrap">
                        <div class="wp-block-uagb-container uagb-block-3035d81c">
                        <div class="wp-block-uagb-image uagb-block-3fe0b2c7 wp-block-uagb-image--layout-default wp-block-uagb-image--effect-static wp-block-uagb-image--align-none">
                          <div class="content-wrapper" style="margin-left: 0px;">
                            <div class="content-wrapper" style="margin-left: 0px;">
                             <div class="content-wrapper" style="margin-left: 0px;">
                                <section class="content-header">
                                  <div class="container">
                                    <div class="row latest-job margin-top-50 margin-bottom-20">
                                      <h1 class="text-center margin-bottom-20">Sign-Up</h1>
                                      <div class="col-md-6 latest-job ">
                                        <div class="small-box bg-red padding-5">
                                          <div class="inner">
                                            <h3 class="text-center">Sender Sign-Up</h3>
                                          </div>
                                          <p class="mb-3">
                                          </p>
                                          <p class="mb-3">
                                          </p>
                                          <p class="mb-3">
                                          </p>
                                          <a href="register-sender.php" class="small-box-footer">
                                          Sign-Up <i class="fa fa-arrow-circle-right"></i>
                                          </a>
                                        </div>
                                      </div>
                                      <div class="col-md-6 latest-job ">
                                        <div class="small-box bg-yellow padding-5">
                                          <div class="inner">
                                            <h3 class="text-center">Traveler Sign-Up</h3>
                                          </div>
                                          <p class="mb-3">
                                          </p>
                                          <p class="mb-3">
                                          </p>
                                          <p class="mb-3">
                                          </p>
                                          <a href="register-traveler.php" class="small-box-footer">
                                          Sign-Up <i class="fa fa-arrow-circle-right"></i>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </section>
                              </div>
                           </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
                            <!-- /.content-wrapper -->
                                <!-- /.content-wrapper -->
                                <?php include 'footer.php';?>
                                <!-- /.control-sidebar -->
                                <!-- Add the sidebar's background. This div must be placed
                                      immediately after the control sidebar -->
                                <div class="control-sidebar-bg">
                              </div>
                              </div>
                              <!-- ./wrapper -->
                              <!-- jQuery 3 -->
                              <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                              <!-- Bootstrap 3.3.7 -->
                              <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
                              <!-- AdminLTE App -->
                              <script src="js/adminlte.min.js"></script>
                              </body>
                              </html>

